# Copyright 2009, 2011, 2013 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix="tar.xz" ]
require gsettings gtk-icon-cache freedesktop-desktop
require cmake

SUMMARY="Personal/groupwork information manager (e-mail, adressbook, calendar, tasks etc.)"
HOMEPAGE="http://projects.gnome.org/${PN}"

SLOT="0"
LICENCES="( LGPL-2 LGPL-3 )"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    bogofilter   [[ description = [ Enable spam filtering using Bogofilter ] ]]
    gspell       [[ description = [ Enable spell checking using gspell ] ]]
    gtk-doc
    help         [[ description = [ Install the user documentation ] ]]
    ldap         [[ description = [ Enable LDAP support in evolution ] ]]
    spamassassin [[ description = [ Enable spam filtering using SpamAssassin ] ]]
    weather      [[ description = [ Build the weather calendar plugin ] ]]

    ( linguas: af am an ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia cs cy da de dz el
               en_AU en_CA en_GB en@shaw eo es et eu fa fi fr ga gl gu he hi hr hu id is it ja ka kk kn
               ko ku lt lv mai mk ml mn mr ms nb nds ne nl nn oc or pa pl ps pt pt_BR ro ru rw si sk
               sl sq sr sr@latin sv ta te tg th tr ug uk vi wa xh zh_CN zh_HK zh_TW )
"
# Currently disabled because champlain-gtk is using libsoup2
#    geolocation [[
#        description = [ Adds a map showing the locations of your contacts ]
#    ]]

# pst-import plugin needs libpst

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        gnome-desktop/yelp-tools
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        app-spell/enchant:2[>=2.2.0]
        app-text/iso-codes[>=0.49]
        dev-db/sqlite:3[>=3.7.17]
        dev-libs/atk
        dev-libs/glib:2[>=2.66.0]
        dev-libs/libsecret:1 [[ note = [ No configure check, used in src/e-util/e-passwords.c ] ]]
        dev-libs/libxml2:2.0[>=2.7.3]
        dev-libs/nspr
        dev-libs/nss
        gnome-desktop/gnome-autoar[>=0.1.1]
        gnome-desktop/evolution-data-server:1.2[~>$(ever range 1-3)][providers:gtk3][weather?][ldap?]
        gnome-desktop/gnome-desktop:4[>=2.91.3][legacy]
        gnome-desktop/gsettings-desktop-schemas[>=2.91.92]
        gnome-desktop/libsoup:3.0[>=3.0]
        media-libs/libcanberra[>=0.25][providers:gtk3]
        net-libs/webkit:4.1[>=2.34.0]
        office-libs/libical:3 [[ note = [ Automagic ] ]]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[>=2.24.0]
        x11-libs/gtk+:3[>=3.22.0]
        x11-libs/libnotify[>=0.7] [[ note = [ Automagic ] ]]
        x11-libs/pango
        x11-misc/shared-mime-info[>=0.22]
        bogofilter? ( mail-filter/bogofilter )
        gspell? ( gnome-desktop/gspell:1 )
        ldap? ( net-directory/openldap[>=2.4.0] )
        spamassassin? ( mail-filter/spamassassin )
        weather? (
            gnome-desktop/geocode-glib:2.0[>=3.26.3]
            gnome-desktop/libgweather:4[>=3.91][providers:soup3]
        )
"
#        geolocation? (
#            gnome-desktop/geocode-glib:2.0[>=3.26.3]
#            x11-libs/clutter-gtk:1.0[>=0.90]
#            x11-libs/libchamplain:0.12
#        )

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DSHARE_INSTALL_PREFIX:STRING=/usr/share
    -DSYSCONF_INSTALL_DIR:STRING=/etc
    -DENABLE_AUTOAR:BOOL=TRUE
    -DENABLE_CANBERRA:BOOL=TRUE
    -DENABLE_GNOME_DESKTOP:BOOL=TRUE
    -DENABLE_MARKDOWN:BOOL=FALSE
    -DENABLE_PLUGINS:STRING=all
    -DENABLE_SMIME:BOOL=TRUE
    -DENABLE_PST_IMPORT:BOOL=FALSE
    -DENABLE_TEXT_HIGHLIGHT:BOOL=FALSE
    -DENABLE_YTNEF:BOOL=FALSE
    -DWITH_ENCHANT_VERSION:STRING=2
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'bogofilter WITH_BOGOFILTER'
    'gspell ENABLE_GSPELL'
    'spamassassin WITH_SPAMASSASSIN'
    'weather ENABLE_WEATHER'
    'help WITH_HELP'
    'ldap WITH_OPENLDAP'
)
# 'geolocation ENABLE_CONTACT_MAPS'

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

