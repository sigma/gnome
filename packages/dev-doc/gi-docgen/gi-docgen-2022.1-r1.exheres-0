# Copyright 2021 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org python [ has_bin=true blacklist=2 multibuild=false ] meson

SUMMARY="A documentation generator for GObject-based libraries"
DOWNLOADS="https://gitlab.gnome.org/GNOME/${PN}/-/archive/${PV}/${PNV}.tar.bz2"

LICENCES="Apache-2.0 CCPL-Attribution-ShareAlike-3.0 CC0 GPL-3 OFL-1.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/Jinja2[python_abis:*(-)?]
        dev-python/Markdown[python_abis:*(-)?]
        dev-python/MarkupSafe[python_abis:*(-)?]
        dev-python/Pygments[python_abis:*(-)?]
        dev-python/toml[python_abis:*(-)?]
        dev-python/typogrify[python_abis:*(-)?]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddevelopment_tests=false
)

src_prepare() {
    meson_src_prepare

    edo sed -e "s/'python3'/'python$(python_get_abi)'/" -i meson.build

    # Fix shebang
    edo sed -e "s:#!/usr/bin/env python3:#!/usr/bin/env python$(python_get_abi):g" \
            -i gi-docgen.py
}

