# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome-bluetooth

SUMMARY="A fork of bluez-gnome focused on integration with the GNOME Desktop"
HOMEPAGE="https://live.gnome.org/GnomeBluetooth"

LICENCES="( GPL-2 LGPL-2.1 )"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc

    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxml2:2.0 [[ note = [ required for xmllint ] ]]
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libadwaita:1[>=1.1][gobject-introspection?]
        gnome-desktop/gsound
        x11-libs/gtk:4.0[>=4.4]
        x11-libs/libnotify[>=0.7.0]
        sys-apps/upower[>=0.99.14][gobject-introspection?]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        !gnome-desktop/gnome-bluetooth:1[<3.34.5-r1] [[
            description = [ Used to install bluetooth-sento too ]
            resolution = upgrade-blocked-before
        ]]
    run:
        group/plugdev [[ note = [ for rfkill udev rules ] ]]
        net-wireless/bluez[>=5.51]
        gnome-desktop/gnome-settings-daemon:3.0
"

# With g-i option enabled and dbusmock installed a bunch of tests fail
RESTRICT=test

MESON_SOURCE="${WORKBASE}/${PNV/_/.}"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dsendto=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    'gobject-introspection introspection'
)

