# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] \
    meson \
    vala [ vala_dep=true with_opt=true ]

SUMMARY="A VNC viewer widget for GTK"
DESCRIPTION="
GTK-VNC is a VNC viewer widget for GTK+. It is built using coroutines, allowing
it to be completely asynchronous while remaining single threaded. It supports RFB
protocols 3.3 through 3.8 and the VeNCrypt authentication extension providing
SSL/TLS encryption with x509 certificate authentication. The core library is
written in C and a binding for Python using PyGTK is available. The networking
layer supports connections over both IPv4 and IPv6. Example code illustrates how
to build a vncviewer replacement using either C or Python.
"
HOMEPAGE="http://live.gnome.org/${PN}"

LICENCES="LGPL-2.1"
MYOPTIONS="
    gobject-introspection
    pulseaudio
"

# fails, last checked: 1.0.0
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/perl:* [[ note = [ for pod2man ] ]]
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.56.0] )
    build+run:
        dev-libs/glib:2[>=2.56.0]
        dev-libs/gnutls[>=3.6.0]
        dev-libs/libgcrypt[>=1.8.0]
        net-libs/cyrus-sasl[>=2.1.27]
        sys-libs/zlib[>=1.2.11]
        x11-libs/cairo[>=1.15.0]
        x11-libs/gdk-pixbuf:2.0[>=2.36.0]
        x11-libs/gtk+:3[>=3.22.0][gobject-introspection]
        x11-libs/libX11[>=1.6.5]
        pulseaudio? ( media-sound/pulseaudio[>=11.0] )
        !dev-libs/gtk-vnc:1 [[
            description = [ Libraries and binaries collide with these slots ]
            resolution = uninstall-blocked-after
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dwith-coroutine=gthread
    -Dwith-tls-priority=NORMAL
    -Dsasl=enabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
    pulseaudio
    'vapi with-vala'
)

