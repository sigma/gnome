# Copyright 2022 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ with_opt=true ]
require meson

SUMMARY="Library providing a GTK4 widget to display maps"
HOMEPAGE="https://wiki.gnome.org/Projects/libshumate"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc

    gtk-doc [[ requires = gobject-introspection ]]
    vapi [[ requires = gobject-introspection ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.3] )
        gtk-doc? ( dev-doc/gi-docgen[>=2021.1] )
    build+run:
        dev-db/sqlite:3[>=1.12.0]
        dev-libs/glib:2[>=2.68.0]
        gnome-desktop/libsoup:3.0
        x11-libs/cairo[>=1.4]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/graphene:1.0
        x11-libs/gtk:4.0[>=4.6]
        x11-libs/pango
"

# Require display access
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddemos=false
    -Dlibsoup3=true

    # It's experimental
    -Dvector_renderer=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection gir'
    'gtk-doc gtk_doc'
    vapi
)

pkg_setup() {
    meson_pkg_setup
    vala_pkg_setup
}

